FROM golang:1.9.1-stretch as build


RUN go get -u github.com/kardianos/govendor

ADD . /go/src/gitlab.com/reading-room/www
WORKDIR /go/src/gitlab.com/reading-room/www

RUN govendor sync

RUN go test -v ./...

RUN go build -o readingroom

#######

FROM debian:stretch as runtime

ENV AUTH0_CLIENT_ID ''
ENV AUTH0_DOMAIN ''
ENV AUTH0_CLIENT_SECRET ''
ENV AUTH0_CALLBACK_URL ''
ENV AUTH0_AUDIENCE ''
ENV HMAC_HASHKEY ''
ENV AES_BLOCKKEY ''

RUN apt-get update && apt-get install -y ca-certificates && update-ca-certificates

USER 1000

WORKDIR /usr/local/bin

COPY --from=build /go/src/gitlab.com/reading-room/www/readingroom /usr/local/bin/
COPY --from=build /go/src/gitlab.com/reading-room/www/assets /usr/local/bin/assets

CMD readingroom
