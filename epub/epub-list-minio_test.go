package epub

import (
	"errors"
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/reading-room/www/env"
)

type EPubMinioSuite struct {
	suite.Suite
	facade *mockingMinioFacade
	logger logrus.FieldLogger

	minioList MinioEPubList
}

func (suite *EPubMinioSuite) SetupTest() {
	suite.facade = &mockingMinioFacade{}

	nullDevice, _ := os.Open(os.DevNull)
	suite.logger = &logrus.Logger{
		Out: nullDevice,
	}

	suite.minioList = MinioEPubList{suite.facade, suite.logger}
}

func epubInfoChan(infos []epubInfo) chan epubInfo {
	c := make(chan epubInfo, 2)
	go func() {
		for _, info := range infos {
			c <- info
		}
		close(c)
	}()
	return c
}

func (suite *EPubMinioSuite) TestFailCreateRepositoryWhenNoMinio() {
	var ENV env.Env
	factory := func(env.Env) (MinioFacade, error) {
		return nil, errors.New("Fail")
	}
	_, err := CreateMinioEPubRepository(ENV, suite.logger, factory)
	suite.NotNil(err)
}
func (suite *EPubMinioSuite) TestListAllBooksRetrievesBooksInfoInMinio() {
	suite.facade.On("List", suite.logger).Return(epubInfoChan([]epubInfo{epubInfo{Key: "ID-1", ETag: "123"}, epubInfo{Key: "ID-2", ETag: "456"}}))
	epub1 := &fakeepub{title: "Title 1", authors: newAuthors("Author 1"), id: "ID-1"}
	epub2 := &fakeepub{title: "Title 2", authors: newAuthors("Author 2"), id: "ID-2"}
	suite.facade.On("DownloadEpub", "ID-1", suite.logger).Return(epub1)
	suite.facade.On("DownloadEpub", "ID-2", suite.logger).Return(epub2)

	suite.Equal([]EPub{epub1, epub2}, suite.minioList.All())
	suite.facade.AssertExpectations(suite.T())
}
func (suite *EPubMinioSuite) TestLoadDownloadFile() {
	epub := &fakeepub{title: "Title 1", authors: newAuthors("Author 1"), id: "ID"}
	suite.facade.On("DownloadEpub", "ID", suite.logger).Return(epub)

	suite.Equal(epub, suite.minioList.Load("ID"))
	suite.facade.AssertExpectations(suite.T())
}

func TestMinioSuite(t *testing.T) {
	suite.Run(t, new(EPubMinioSuite))
}

type mockingMinioFacade struct {
	mock.Mock
}

func (m *mockingMinioFacade) List(logger logrus.FieldLogger) <-chan epubInfo {
	args := m.Called(logger)
	return args.Get(0).(chan epubInfo)
}
func (m *mockingMinioFacade) DownloadEpubThumbnail(name string, logger logrus.FieldLogger) Thumbnail {
	args := m.Called(name, logger)
	return args.Get(0).(Thumbnail)
}
func (m *mockingMinioFacade) DownloadEpub(name string, logger logrus.FieldLogger) EPub {
	args := m.Called(name, logger)
	return args.Get(0).(EPub)
}
