package epub

import (
	"os"

	"github.com/sirupsen/logrus"
)

type fileSystemThumbnail struct {
	epub EPub
}

func (th fileSystemThumbnail) Title() string {
	return th.epub.Title()[0]
}
func (th fileSystemThumbnail) Author() string {
	if len(th.epub.Author()) == 0 {
		return "NO_AUTHOR"
	}
	return th.epub.Author()[0].Name()
}
func (th fileSystemThumbnail) Id() string {
	return th.epub.Id()
}

type FileSystemEPubRepository struct {
	DirectoryPath string
	Logger        logrus.FieldLogger
}

func (fs FileSystemEPubRepository) All() []EPub {
	result := []EPub{}
	f, err := os.Open(fs.DirectoryPath)
	if err != nil {
		fs.Logger.WithField("dirname", fs.DirectoryPath).WithError(err).Warn("FileSystemEPubRepository - Open directory")
	}
	defer f.Close()
	infos, err := f.Readdir(0)
	if err != nil {
		fs.Logger.WithError(err).Warn("FileSystemEPubRepository - Read directory")
	}
	for _, info := range infos {
		ext := info.Name()[len(info.Name())-5:]
		if ext == ".epub" {
			result = append(result, fileEPubLoader{fs.DirectoryPath + "/" + info.Name()}.Load(info.Name()))
		}
	}
	return result
}
func (fs FileSystemEPubRepository) Load(id string) EPub {
	f, err := os.Open(fs.DirectoryPath)
	if err != nil {
		fs.Logger.WithField("dirname", fs.DirectoryPath).WithError(err).Warn("FileSystemEPubRepository - Open directory")
	}
	defer f.Close()
	return fileEPubLoader{fs.DirectoryPath + "/" + id}.Load(id)
}
