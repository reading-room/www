package epub

import (
	"io/ioutil"
	"os"

	minio "github.com/minio/minio-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/reading-room/www/env"
)

type MinioFacade interface {
	List(logrus.FieldLogger) <-chan epubInfo
	DownloadEpub(name string, logger logrus.FieldLogger) EPub
}

type epubInfo struct {
	Key  string
	ETag string
}

type clientMinioFacade struct {
	minioClient *minio.Client
}

func (f clientMinioFacade) List(log logrus.FieldLogger) <-chan epubInfo {
	objectCh := f.minioClient.ListObjects("readingroom", "", true, nil)
	result := make(chan epubInfo)
	go func() {
		for object := range objectCh {
			if object.Err != nil {
				log.WithError(object.Err).Warn("List files")
				close(result)
			}
			result <- epubInfo{object.Key, object.ETag}
		}
		close(result)
	}()
	return result
}

func (f clientMinioFacade) DownloadEpub(name string, log logrus.FieldLogger) EPub {
	tempFile, err := ioutil.TempFile("", "epub")
	if err != nil {
		log.WithError(err).Error("Impossible to create temp file")
		return nil
	}
	log.Infof("Created file %s\n", tempFile.Name())
	defer os.Remove(tempFile.Name())
	f.minioClient.FGetObject("readingroom", name, tempFile.Name(), minio.GetObjectOptions{})
	return fileEPubLoader{tempFile.Name()}.Load(name)
}

func NewFacade(env env.Env) (MinioFacade, error) {
	minioClient, err := minio.New("minio:9000", env.String("MINIO_ACCESS_KEY"), env.String("MINIO_SECRET_ACCESS_KEY"), false)
	if err != nil {
		return nil, err
	}
	_, err = minioClient.ListBuckets() // Just a test for ckecking connection
	return clientMinioFacade{minioClient}, err
}
