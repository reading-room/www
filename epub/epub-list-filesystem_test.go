package epub

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

type fakeauthor struct {
	name string
	role string
}

func (auth *fakeauthor) Name() string {
	return auth.name
}
func (auth *fakeauthor) Role() string {
	return auth.role
}

type fakeepub struct {
	id      string
	title   string
	authors []Author
}

func (epub *fakeepub) Id() string {
	return epub.id
}
func (epub *fakeepub) Title() []string {
	return []string{epub.title}
}
func (epub *fakeepub) Author() []Author {
	return epub.authors
}
func (epub *fakeepub) Ncx() Ncx {
	return nil
}
func (epub *fakeepub) Get(ref ResourceReference) (io.ReadCloser, error) {
	return nil, nil
}

func epubWithAuthors(authors ...Author) EPub {
	return &fakeepub{authors: authors}
}
func newAuthors(names ...string) []Author {
	result := make([]Author, len(names))
	for i, name := range names {
		result[i] = &fakeauthor{name: name}
	}
	return result
}

func TestRetrieveId(t *testing.T) {
	title := "Epub Title 1"
	thumbnail := fileSystemThumbnail{&fakeepub{id: "123456", title: title}}
	assert.Equal(t, "123456", thumbnail.Id())
}
func TestShowOnlyFirstAuthor(t *testing.T) {
	thumbnail := fileSystemThumbnail{epubWithAuthors(&fakeauthor{name: "Stephen King", role: "Writer"}, &fakeauthor{name: "Jean Bonnefoy", role: "Translater"})}
	assert.Equal(t, "Stephen King", thumbnail.Author())
}
func TestShowNoAuthorIfNoAuthros(t *testing.T) {
	thumbnail := fileSystemThumbnail{epubWithAuthors()}
	assert.Equal(t, "NO_AUTHOR", thumbnail.Author())
}
