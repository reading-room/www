package epub

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemoveSharpPart(t *testing.T) {
	ref := ResourceReference{"abc#def"}
	if ref.Get() != "abc" {
		t.Errorf("Expected <%s> but was <%s>", "abc", ref.Get())
	}
}

func TestDoesNothingIfStringNotContainingSharp(t *testing.T) {
	ref := ResourceReference{"abc"}
	if ref.Get() != "abc" {
		t.Errorf("Expected <%s> but was <%s>", "abc", ref.Get())
	}
}

func TestNewThumbnail(t *testing.T) {
	epub := &fakeepub{id: "123456", title: "Title", authors: []Author{&fakeauthor{name: "Fry Ter", role: "Writer"}}}
	assert.Equal(t, Thumbnail{Id: "123456", Title: "Title", Author: "Fry Ter"}, NewThumbnail(epub))
}
func TestNewThumbnailWithNoTitle(t *testing.T) {
	epub := &fakeepub{id: "123456", authors: []Author{&fakeauthor{name: "Fry Ter", role: "Writer"}}}
	assert.Equal(t, Thumbnail{Id: "123456", Title: "NO_TITLE", Author: "Fry Ter"}, NewThumbnail(epub))
}
func TestNewThumbnailWithNoAuthor(t *testing.T) {
	epub := &fakeepub{id: "123456", title: "Title"}
	assert.Equal(t, Thumbnail{Id: "123456", Title: "Title", Author: "NO_AUTHOR"}, NewThumbnail(epub))
}

func TestNewThumbnails(t *testing.T) {
	epub1 := &fakeepub{id: "123456", title: "Title 1", authors: []Author{&fakeauthor{name: "Fry Ter", role: "Writer"}}}
	epub2 := &fakeepub{id: "789123", title: "Title 2", authors: []Author{&fakeauthor{name: "W. Riter", role: "Writer"}}}
	assert.Equal(t, []Thumbnail{
		Thumbnail{Id: "123456", Title: "Title 1", Author: "Fry Ter"},
		Thumbnail{Id: "789123", Title: "Title 2", Author: "W. Riter"},
	}, NewThumbnails([]EPub{epub1, epub2}))
}
