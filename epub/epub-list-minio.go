package epub

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/reading-room/www/env"
)

type MinioEPubList struct {
	facade MinioFacade
	logger logrus.FieldLogger
}

func (list MinioEPubList) All() []EPub {
	result := []EPub{}
	ch := list.facade.List(list.logger)
	for object := range ch {
		book := list.facade.DownloadEpub(object.Key, list.logger)
		result = append(result, book)
	}
	return result
}
func (list MinioEPubList) Load(id string) EPub {
	book := list.facade.DownloadEpub(id, list.logger)
	return book
}

func CreateMinioEPubRepository(env env.Env, logger logrus.FieldLogger, facadeFactory func(env.Env) (MinioFacade, error)) (EPubRepository, error) {
	facade, err := facadeFactory(env)
	if err != nil {
		logger.WithError(err).Error("Impossible to create facade")
		return nil, err
	}
	logger.Info("Connected to Minio")
	return MinioEPubList{facade, logger}, nil
}
