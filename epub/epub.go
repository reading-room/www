package epub

import (
	"fmt"
	"io"
	"strings"

	"github.com/lioda/epub"
)

type ResourceReference struct {
	Ref string
}

func (r ResourceReference) Get() string {
	return strings.Split(r.Ref, "#")[0]
}

type Thumbnail struct {
	Title  string
	Author string
	Id     string
}

func NewThumbnail(book EPub) Thumbnail {
	thumbnail := Thumbnail{Id: book.Id()}
	if title := book.Title()[0]; title == "" {
		thumbnail.Title = "NO_TITLE"
	} else {
		thumbnail.Title = title
	}
	if len(book.Author()) > 0 {
		thumbnail.Author = book.Author()[0].Name()
	} else {
		thumbnail.Author = "NO_AUTHOR"
	}
	return thumbnail
}
func NewThumbnails(books []EPub) []Thumbnail {
	result := make([]Thumbnail, len(books))
	for i, book := range books {
		result[i] = NewThumbnail(book)
	}
	return result
}

type EPub interface {
	Id() string
	Title() []string
	Author() []Author
	Ncx() Ncx
	Get(ref ResourceReference) (io.ReadCloser, error)
}

type Ncx interface {
	NavPoints() []NavPoint
}

type Author interface {
	Name() string
	Role() string
}

type NavPoint interface {
	Id() string
	Text() string
	ContentRef() string
	NavPoints() []NavPoint
}

/////////////// epub wrapper

// EPUB
type epubWrapper struct {
	id   string
	book *epub.Book
}

func (e epubWrapper) Id() string {
	return e.id
}
func (e epubWrapper) Ncx() Ncx {
	return ncxWrapper{e.book.Ncx}
}
func (e epubWrapper) Title() []string {
	return e.book.Opf.Metadata.Title
}
func (e epubWrapper) Author() []Author {
	result := []Author{}
	for _, author := range e.book.Opf.Metadata.Creator {
		result = append(result, authorWrapper{author})
	}
	// return authorWrapper{e.book.Opf.Metadata.Creator[0]}
	return result
}
func (e epubWrapper) Get(ref ResourceReference) (io.ReadCloser, error) {
	return e.book.Open(ref.Get())
}

type authorWrapper struct {
	author epub.Author
}

func (a authorWrapper) Name() string {
	return a.author.Data
}
func (a authorWrapper) Role() string {
	return a.author.Role
}

// NCX
type ncxWrapper struct {
	ncx epub.Ncx
}

func (n ncxWrapper) NavPoints() []NavPoint {
	return mapToWrapper(n.ncx.Points)
}

// NavPoint
type navPointWrapper struct {
	navpoint epub.NavPoint
}

func (np navPointWrapper) Id() string {
	return np.navpoint.Id
}
func (np navPointWrapper) Text() string {
	return np.navpoint.Text
}
func (np navPointWrapper) ContentRef() string {
	return np.navpoint.Content.Src
}
func (np navPointWrapper) NavPoints() []NavPoint {
	return mapToWrapper(np.navpoint.Points)
}

// Mapping
func mapToWrapper(navpoints []epub.NavPoint) []NavPoint {
	var result []NavPoint
	for _, navpoint := range navpoints {
		result = append(result, navPointWrapper{navpoint})
	}
	return result
}

type fileEPubLoader struct {
	Filename string
}

func (s fileEPubLoader) Load(id string) EPub {
	fmt.Printf("Opening %s\n", s.Filename)
	book, error := epub.Open(s.Filename)
	if error != nil {
		panic(error)
	}
	return epubWrapper{id, book}
}
