package epub

type EPubRepository interface {
	All() []EPub
	Load(id string) EPub
}
