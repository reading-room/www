package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOsEnvGetEnvValueFromOs(t *testing.T) {
	os.Setenv("MY_KEY", "MY_VALUE")
	assert.Equal(t, "MY_VALUE", osEnv{}.String("MY_KEY"))
}

type concrete struct {
	s string
}
type mockFactory struct {
}

func (f mockFactory) Build(e Env) concrete {
	return concrete{e.String("KEY")}
}

func TestWhenOsEnvBuildByFactoryThenReturnFactoryResult(t *testing.T) {
	os.Setenv("KEY", "VALUE")
	assert.Equal(t, concrete{"VALUE"}, mockFactory{}.Build(osEnv{}))
}
