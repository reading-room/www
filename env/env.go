package env

import "os"

type EnvFactory interface {
	Build() interface{}
}

type Env interface {
	String(string) string
}

type osEnv struct {
}

func (o osEnv) String(key string) string {
	return os.Getenv(key)
}

func OsEnv() Env {
	return osEnv{}
}
