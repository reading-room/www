package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/reading-room/www/cookies"
	"gitlab.com/reading-room/www/env"
	"gitlab.com/reading-room/www/epub"
	"gitlab.com/reading-room/www/log"
	"gitlab.com/reading-room/www/oauth"
)

func healthcheckHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello internal voice")
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/favicon.ico")
}

type HomeTemplater struct {
	cookies        cookies.Cookies
	epubRepository epub.EPubRepository
}

func (h HomeTemplater) homeHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("assets/index.html")
	userInfo := new(oauth.UserInfo)
	data := map[string]interface{}{
		"User":  userInfo,
		"Books": epub.NewThumbnails(h.epubRepository.All()),
	}
	h.cookies.Get(r, "user-info", userInfo)
	t.Execute(w, data)
}

func logHandler(logger logrus.FieldLogger, h func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.WithField("path", r.URL.RequestURI()).Info("AccessLog")
		h(w, r)
	}
}
func errorHandler(logger logrus.FieldLogger, h func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.WithField("path", r.URL.RequestURI()).Error("AccessLog")
		h(w, r)
	}
}

func protectedHandler(now time.Time, auth oauth.Authentication, h func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if auth.IsAuthenticated(r, now) {
			h(w, r)
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	}
}

type epubHandlers struct {
	// Loader     epub.EPubLoader
	Repository epub.EPubRepository
}

//
// func (h epubHandlers) resourceHandler(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	epub := h.Loader.Load(vars["epubId"])
// 	// epubDisplayer := displayer.Displayer{Epub: epub}
// 	// navpointContent, _ := epubDisplayer.Display(vars["resourceId"])
// 	fmt.Fprint(w, navpointContent)
// }
func (h epubHandlers) tableOfContentHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	book := h.Repository.Load(vars["epubId"])
	// displayer := displayer.Displayer{epub}
	// navpointContent, _ := displayer.Display(vars["resourceId"])
	// fmt.Fprint(w, navpointContent)
	t, _ := template.ParseFiles("assets/toc.html")
	t.Execute(w, struct {
		Title     string
		NavPoints []epub.NavPoint
	}{
		Title:     book.Title()[0],
		NavPoints: h.flattenNavPoints(book.Ncx().NavPoints()),
	})
}

func (h epubHandlers) flattenNavPoints(navpoints []epub.NavPoint) []epub.NavPoint {
	result := []epub.NavPoint{}
	for _, navpoint := range navpoints {
		result = append(result, navpoint)
		result = append(result, h.flattenNavPoints(navpoint.NavPoints())...)
	}
	return result
}

func Serve(auth oauth.Authentication, now time.Time, cookies cookies.Cookies, epubList epub.EPubRepository, logger logrus.FieldLogger) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/favicon.ico", logHandler(logger, faviconHandler))
	r.HandleFunc("/v1/healthcheck", logHandler(logger, healthcheckHandler))
	r.HandleFunc("/login", logHandler(logger, auth.LoginHandler))
	r.HandleFunc("/callback", logHandler(logger, auth.CallbackHandler))

	handlers := epubHandlers{epubList}
	// r.HandleFunc("/v1/read/{epubId}/{resourceId}", logHandler(logger, protectedHandler(now, auth, handlers.resourceHandler)))
	r.HandleFunc("/v1/read/{epubId}", logHandler(logger, protectedHandler(now, auth, handlers.tableOfContentHandler)))

	templater := HomeTemplater{cookies, epubList}
	r.HandleFunc("/", logHandler(logger, protectedHandler(now, auth, templater.homeHandler)))
	r.HandleFunc("/{unknown:.*}", errorHandler(logger, http.NotFound))
	return r
}

func main() {
	osenv := env.OsEnv()
	logger := log.InitLogger(osenv)
	logger.Info("start")
	cookieStore := cookies.CreateCookie(cookies.Configure(osenv), logger)
	stateGenerator := oauth.RandomStateGenerator{}
	auth := oauth.Configure(cookieStore, osenv, logger, stateGenerator)
	epublist, err := epub.CreateMinioEPubRepository(osenv, logger, epub.NewFacade)
	if err != nil {
		logger.WithError(err).Warn("Degraded mode, Minio unreachable")
		epublist = epub.FileSystemEPubRepository{DirectoryPath: "./assets", Logger: logger}
	}
	http.Handle("/", Serve(auth, time.Now(), cookieStore, epublist, logger))
	http.ListenAndServe(":8080", nil)
	logger.Warn("end")
}
