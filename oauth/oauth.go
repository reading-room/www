package oauth

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/reading-room/www/cookies"
	"gitlab.com/reading-room/www/env"
)

type Authentication interface {
	IsAuthenticated(*http.Request, time.Time) bool
	LoginHandler(w http.ResponseWriter, r *http.Request)
	CallbackHandler(w http.ResponseWriter, r *http.Request)
}

type StateGenerator interface {
	CreateBase64() string
}

type Token struct {
	AccessToken  string
	TokenType    string
	RefreshToken string
	Expiry       time.Time
}

func (t Token) Valid(now time.Time) bool {
	return t.AccessToken != "" && !now.After(t.Expiry)
}

type UserInfo struct {
	Email     string    `json:"email"`
	UserId    string    `json:"user_id"`
	Name      string    `json:"name"`
	Nickname  string    `json:"nickname"`
	Picture   string    `json:"picture"`
	LastLogin time.Time `json:"last_login"`
}

// Configure authentication mode
func Configure(cookies cookies.Cookies, e env.Env, logger logrus.FieldLogger, stateGenerator StateGenerator) Authentication {
	if hasBadConfiguration(e) {
		return noAuth{cookies}
	}
	return Auth0Authentication{cookies, stateGenerator, e, logger}
}
func hasBadConfiguration(e env.Env) bool {
	return e.String("AUTH0_CLIENT_ID") == "" ||
		e.String("AUTH0_DOMAIN") == "" ||
		e.String("AUTH0_CLIENT_SECRET") == "" ||
		e.String("AUTH0_CALLBACK_URL") == "" ||
		e.String("AUTH0_AUDIENCE") == ""
}
