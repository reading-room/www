package oauth

import (
	"context"
	"encoding/json"
	"io"

	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

type oauth2conf interface {
	AuthCodeURL(state string, audience string) string
	Exchange(ctx context.Context, code string) (*Token, error)
	UserInfo(ctx context.Context, token Token) *UserInfo
}

type oauth2confWrapper struct {
	Conf   oauth2.Config
	Domain string
}

func (w oauth2confWrapper) AuthCodeURL(state string, audience string) string {
	audienceParam := oauth2.SetAuthURLParam("audience", audience)
	return w.Conf.AuthCodeURL(state, audienceParam)
}
func (w oauth2confWrapper) Exchange(ctx context.Context, code string) (*Token, error) {
	tk, err := w.Conf.Exchange(ctx, code)
	if err != nil {
		logrus.WithFields(logrus.Fields{"code": code}).WithError(err).Error("Auth0 Exchange")
		return nil, err
	}
	token := Token{
		AccessToken:  tk.AccessToken,
		TokenType:    tk.TokenType,
		RefreshToken: tk.RefreshToken,
		Expiry:       tk.Expiry,
	}
	return &token, nil
}
func (w oauth2confWrapper) UserInfo(ctx context.Context, token Token) *UserInfo {
	client := w.Conf.Client(ctx, &oauth2.Token{
		AccessToken:  token.AccessToken,
		Expiry:       token.Expiry,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
	})
	resp, err := client.Get("https://" + w.Domain + "/userinfo")
	if err != nil {
		logrus.WithFields(logrus.Fields{"url": "https://" + w.Domain + "/userinfo"}).WithError(err).Error("Auth0 UserInfo")
	}
	defer resp.Body.Close()
	return decodeJson(resp.Body)
}

func decodeJson(reader io.Reader) *UserInfo {
	info := new(UserInfo)
	json.NewDecoder(reader).Decode(&info)
	return info
}
