package oauth

import (
	"crypto/rand"
	"encoding/base64"
)

type RandomStateGenerator struct {
}

func (r RandomStateGenerator) CreateBase64() string {
	b := make([]byte, 32)
	rand.Read(b)
	state := base64.StdEncoding.EncodeToString(b)
	return state
}
