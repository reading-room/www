package oauth

import (
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDecodeJson(t *testing.T) {
	info := decodeJson(strings.NewReader(`{
    "email": "aa@bb.com",
    "user_id": "USER_ID",
    "name": "aa@",
    "nickname": "aa",
    "picture": "https://image.url.com/img.jpg",
    "last_login": "2017-11-23T21:40:50.0Z"
  }`))

	assert.Equal(t, &UserInfo{
		"aa@bb.com",
		"USER_ID",
		"aa@",
		"aa",
		"https://image.url.com/img.jpg",
		time.Date(2017, time.November, 23, 21, 40, 50, 0, time.UTC),
	}, info)
}
