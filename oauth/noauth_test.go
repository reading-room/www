package oauth

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestWhenUserInfoNicknameIsInCookieThenReturnsTrue(t *testing.T) {
	cookies := MockingCookies{}
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	cookies.On("Get", req, "user-info", &UserInfo{}).Run(func(args mock.Arguments) {
		t := args.Get(2)
		userInfo, _ := t.(*UserInfo)
		userInfo.Nickname = "nickname"
	})
	time := time.Date(2017, time.November, 14, 12, 47, 52, 46, time.UTC)

	noauth := noAuth{cookies}
	assert.Equal(t, true, noauth.IsAuthenticated(req, time))
}

func TestWhenUserInfoNicknameIsNotInCookieThenReturnsTrue(t *testing.T) {
	cookies := MockingCookies{}
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	cookies.On("Get", req, "user-info", &UserInfo{}).Run(func(args mock.Arguments) {
	})
	time := time.Date(2017, time.November, 14, 12, 47, 52, 46, time.UTC)

	noauth := noAuth{cookies}
	assert.Equal(t, false, noauth.IsAuthenticated(req, time))
}

func TestWhenLoginHandlerThenCreateDemoUserAndRedirectOnHome(t *testing.T) {
	cookies := MockingCookies{}
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	res := httptest.NewRecorder()
	cookies.On("Set", res, "user-info", &UserInfo{Nickname: "demo"})

	noauth := noAuth{cookies}
	noauth.LoginHandler(res, req)
	assert.Equal(t, 302, res.Code)
	assert.Equal(t, "/", res.Header().Get("Location"))
	cookies.AssertExpectations(t)
}

func TestNoopCallbackHandler(t *testing.T) {
	noauth := noAuth{nil}
	noauth.CallbackHandler(nil, nil)
}
