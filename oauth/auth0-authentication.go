package oauth

import (
	"context"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"

	"gitlab.com/reading-room/www/cookies"
	"gitlab.com/reading-room/www/env"
)

var conf oauth2conf

func oauth2config(env env.Env) oauth2conf {
	if conf == nil {
		domain := env.String("AUTH0_DOMAIN")
		conf = &oauth2confWrapper{oauth2.Config{
			ClientID:     env.String("AUTH0_CLIENT_ID"),
			ClientSecret: env.String("AUTH0_CLIENT_SECRET"),
			RedirectURL:  env.String("AUTH0_CALLBACK_URL"),
			Scopes:       []string{"openid", "profile"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  "https://" + domain + "/authorize",
				TokenURL: "https://" + domain + "/oauth/token",
			},
		}, domain}
	}
	return conf
}

type Auth0Authentication struct {
	Cookies        cookies.Cookies
	StateGenerator StateGenerator
	Env            env.Env
	Logger         logrus.FieldLogger
}

func (a Auth0Authentication) IsAuthenticated(r *http.Request, now time.Time) bool {
	token := Token{}
	a.Cookies.Get(r, "token", &token)
	return token.Valid(now)
}

func (a Auth0Authentication) LoginHandler(w http.ResponseWriter, r *http.Request) {
	state := a.StateGenerator.CreateBase64()
	a.Cookies.Set(w, "state", state)

	url := oauth2config(a.Env).AuthCodeURL(state, a.Env.String("AUTH0_AUDIENCE"))
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (a Auth0Authentication) CallbackHandler(w http.ResponseWriter, r *http.Request) {
	expectedState := ""
	a.Cookies.Get(r, "state", &expectedState)
	if r.URL.Query().Get("state") != expectedState {
		http.Error(w, "Invalid state parameter:", 400)
		return
	}

	token, err := oauth2config(a.Env).Exchange(context.TODO(), r.URL.Query().Get("code"))
	if err != nil {
		a.Logger.WithError(err).Error("CallbackHandler: Token get impossible")
		http.Error(w, "Bad Token Exchange", 500)
		return
	}
	userInfo := oauth2config(a.Env).UserInfo(context.TODO(), *token)
	a.Cookies.Set(w, "user-info", userInfo)
	a.Cookies.Set(w, "token", token)

	http.Redirect(w, r, "/", 302)
}
