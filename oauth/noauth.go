package oauth

import (
	"net/http"
	"time"

	"gitlab.com/reading-room/www/cookies"
)

type noAuth struct {
	cookieStore cookies.Cookies
}

func (noop noAuth) IsAuthenticated(r *http.Request, t time.Time) bool {
	userInfo := UserInfo{}
	noop.cookieStore.Get(r, "user-info", &userInfo)
	return userInfo.Nickname != ""
}
func (noop noAuth) LoginHandler(w http.ResponseWriter, r *http.Request) {
	userInfo := UserInfo{Nickname: "demo"}
	noop.cookieStore.Set(w, "user-info", &userInfo)
	http.Redirect(w, r, "/", 302)
}
func (noop noAuth) CallbackHandler(w http.ResponseWriter, r *http.Request) {}
