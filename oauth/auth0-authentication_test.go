package oauth

import (
	"bytes"
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

///////////////////////// Mock: Cookies

type MockingCookies struct {
	mock.Mock
}

func (c MockingCookies) Get(req *http.Request, key string, result interface{}) bool {
	c.Called(req, key, result)
	return true
}
func (c MockingCookies) Set(res http.ResponseWriter, key string, value interface{}) string {
	c.Called(res, key, value)
	return ""
}

///////////////////////// Mock: Env

type MockingEnv struct {
	mock.Mock
}

func (s MockingEnv) String(k string) string {
	args := s.Called(k)
	return args.String(0)
}

///////////////////////// Mock: StateGenerator

type MockingStateGenerator struct {
	mock.Mock
}

func (s MockingStateGenerator) CreateBase64() string {
	args := s.Called()
	return args.String(0)
}

///////////////////////// Suite

type Auth0Suite struct {
	suite.Suite
	cookies        MockingCookies
	env            MockingEnv
	stateGenerator MockingStateGenerator
	beforeExpiry   time.Time
	afterExpiry    time.Time
	token          Token
	userInfo       UserInfo
	logfile        *bytes.Buffer

	auth Auth0Authentication
}

///////////////////////// Setup

func (suite *Auth0Suite) SetupTest() {
	suite.cookies = MockingCookies{}

	expiry := time.Date(2017, time.November, 14, 12, 47, 52, 46, time.UTC)

	minus10, _ := time.ParseDuration("-10m")
	plus10, _ := time.ParseDuration("10m")

	suite.beforeExpiry = expiry.Add(minus10)
	suite.afterExpiry = expiry.Add(plus10)

	suite.token = Token{
		AccessToken:  "ACESS_TOKEN",
		TokenType:    "TOKEN_TYPE",
		RefreshToken: "REFRESH_TOKEN",
		Expiry:       expiry,
	}

	suite.userInfo = UserInfo{
		Email:     "aa@bb.com",
		Name:      "Aa",
		Picture:   "http://avatar/aa",
		UserId:    "user123",
		LastLogin: time.Date(2017, time.October, 21, 8, 47, 26, 369, time.UTC),
	}

	suite.env = MockingEnv{}
	suite.stateGenerator = MockingStateGenerator{}

	suite.logfile = new(bytes.Buffer)
	lgr := logrus.New()
	lgr.Out = suite.logfile
	logger := lgr.WithField("application_version", "test")

	suite.auth = Auth0Authentication{&suite.cookies, &suite.stateGenerator, &suite.env, logger}
}

///////////////////////// IsAuthenticated

func (suite *Auth0Suite) TestIsAuthenticatedIfCookiesContainAuthSession() {
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	suite.cookies.On("Get", req, "token", &Token{}).Run(func(args mock.Arguments) {
		t := args.Get(2)
		token, _ := t.(*Token)
		token.AccessToken = suite.token.AccessToken
		token.TokenType = suite.token.TokenType
		token.RefreshToken = suite.token.RefreshToken
		token.Expiry = suite.token.Expiry
	})

	suite.True(suite.auth.IsAuthenticated(req, suite.beforeExpiry))
	suite.cookies.AssertExpectations(suite.T())
}

func (suite *Auth0Suite) TestNotIsAuthenticatedIfSessionWithoutToken() {
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	suite.cookies.On("Get", req, "token", &Token{})

	suite.False(suite.auth.IsAuthenticated(req, suite.beforeExpiry))
	suite.cookies.AssertExpectations(suite.T())
}

func (suite *Auth0Suite) TestNotIsAuthenticatedIfTokenExpired() {
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	suite.cookies.On("Get", req, "token", &Token{}).Run(func(args mock.Arguments) {
		t := args.Get(2)
		token, _ := t.(*Token)
		token.AccessToken = suite.token.AccessToken
		token.TokenType = suite.token.TokenType
		token.RefreshToken = suite.token.RefreshToken
		token.Expiry = suite.token.Expiry
	})

	suite.False(suite.auth.IsAuthenticated(req, suite.afterExpiry))
	suite.cookies.AssertExpectations(suite.T())
}

///////////////////////// LoginHandler

func (suite *Auth0Suite) setupEnv() {
	suite.env.On("String", "AUTH0_DOMAIN").Return("AUTH0_DOMAIN")
	suite.env.On("String", "AUTH0_CLIENT_ID").Return("AUTH0_CLIENT_ID")
	suite.env.On("String", "AUTH0_CLIENT_SECRET").Return("AUTH0_CLIENT_SECRET")
	suite.env.On("String", "AUTH0_CALLBACK_URL").Return("https://my.url.com/callback")
	suite.env.On("String", "AUTH0_AUDIENCE").Return("https://AUTH0_DOMAIN/api/v2/")
}

func (suite *Auth0Suite) TestLoginHandlerCreateStateAndSetItInCookies() {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	suite.stateGenerator.On("CreateBase64").Return("__FakeState__")
	suite.cookies.On("Set", res, "state", "__FakeState__")

	suite.setupEnv()

	suite.auth.LoginHandler(res, req)

	suite.cookies.AssertExpectations(suite.T())
	suite.stateGenerator.AssertExpectations(suite.T())
	suite.Equal(307, res.Code)
	suite.Equal("https://AUTH0_DOMAIN/authorize?"+
		"audience=https%3A%2F%2FAUTH0_DOMAIN%2Fapi%2Fv2%2F&"+
		"client_id=AUTH0_CLIENT_ID&"+
		"redirect_uri=https%3A%2F%2Fmy.url.com%2Fcallback&"+
		"response_type=code&"+
		"scope=openid+profile&"+
		"state=__FakeState__",
		res.Header().Get("Location"))
	suite.env.AssertExpectations(suite.T())
}

///////////////////////// CallbackHandler

type MockingConf struct {
	mock.Mock
}

func (c *MockingConf) AuthCodeURL(state string, audience string) string {
	args := c.Called(state, audience)
	return args.String(0)
}
func (c *MockingConf) Exchange(ctx context.Context, code string) (*Token, error) {
	args := c.Called(ctx, code)
	if args.Get(0) == nil {
		return nil, args.Get(1).(error)
	}
	return args.Get(0).(*Token), nil
}
func (c *MockingConf) UserInfo(ctx context.Context, token Token) *UserInfo {
	args := c.Called(ctx, token)
	return args.Get(0).(*UserInfo)
}

func (suite *Auth0Suite) TestCallbackHandlerCheckStateRetrieveTokenAndInfoAndSetItInCookies() {
	confBackup := conf
	mockConf := new(MockingConf)
	conf = mockConf
	mockConf.On("Exchange", context.TODO(), "__FakeCode__").Return(&suite.token, nil)
	mockConf.On("UserInfo", context.TODO(), suite.token).Return(&suite.userInfo)
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/?state=__FakeState__&code=__FakeCode__", nil)
	//suite.setupEnv()
	suite.cookies.On("Get", req, "state", mock.AnythingOfType("*string")).Return(true).Run(func(args mock.Arguments) {
		ptrS := args.Get(2).(*string)
		*ptrS = "__FakeState__"
	})
	suite.cookies.On("Set", res, "user-info", &suite.userInfo).Return("set-cookie-user-info")
	suite.cookies.On("Set", res, "token", &suite.token).Return("set-cookie-token")

	suite.auth.CallbackHandler(res, req)

	suite.Equal(302, res.Code)
	suite.Equal("/", res.Header().Get("Location"))

	suite.env.AssertExpectations(suite.T())
	suite.cookies.AssertExpectations(suite.T())
	conf = confBackup
}

func (suite *Auth0Suite) TestCallbackHandlerFailCheckStateIfCookieAndRequestMismatch() {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/?state=__FakeState__&code=__FakeCode__", nil)
	suite.cookies.On("Get", req, "state", mock.AnythingOfType("*string")).Return(true).Run(func(args mock.Arguments) {
		ptrS := args.Get(2).(*string)
		*ptrS = "__AnotherState__"
	})

	suite.auth.CallbackHandler(res, req)

	suite.Equal(400, res.Code)

	suite.cookies.AssertExpectations(suite.T())
}

func (suite *Auth0Suite) TestCallbackHandlerLogsIfTokenExchangeImpossible() {
	confBackup := conf
	mockConf := new(MockingConf)
	conf = mockConf
	mockConf.On("Exchange", context.TODO(), "__FakeCode__").Return(nil, errors.New("OOPS"))
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/?state=__FakeState__&code=__FakeCode__", nil)
	suite.cookies.On("Get", req, "state", mock.AnythingOfType("*string")).Return(true).Run(func(args mock.Arguments) {
		ptrS := args.Get(2).(*string)
		*ptrS = "__FakeState__"
	})

	suite.auth.CallbackHandler(res, req)

	suite.Equal(500, res.Code)

	suite.env.AssertExpectations(suite.T())
	suite.cookies.AssertExpectations(suite.T())
	suite.Contains(suite.logfile.String(), "CallbackHandler: Token get impossible")
	suite.Contains(suite.logfile.String(), "error=OOPS")
	suite.Contains(suite.logfile.String(), "level=error")
	suite.Contains(suite.logfile.String(), "application_version=test")
	conf = confBackup
}

///////////////////////// Run Suite

func TestRunSuite(t *testing.T) {
	suite.Run(t, new(Auth0Suite))
}
