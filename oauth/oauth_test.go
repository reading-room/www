package oauth

import (
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestTokenValidIfDateBeforeExpiry(t *testing.T) {
	expiry := time.Date(2017, 10, 25, 12, 25, 14, 9, time.UTC)
	minus10minutes, _ := time.ParseDuration("-10m")
	token := Token{
		AccessToken:  "ACCESS",
		TokenType:    "TYPE",
		RefreshToken: "REFRESH",
		Expiry:       expiry,
	}
	assert.Equal(t, true, token.Valid(expiry.Add(minus10minutes)))
}
func TestTokenInvalidIfDateAfterExpiry(t *testing.T) {
	expiry := time.Date(2017, 10, 25, 12, 25, 14, 9, time.UTC)
	plus10minutes, _ := time.ParseDuration("10m")
	token := Token{
		AccessToken:  "ACCESS",
		TokenType:    "TYPE",
		RefreshToken: "REFRESH",
		Expiry:       expiry,
	}
	assert.Equal(t, false, token.Valid(expiry.Add(plus10minutes)))
}
func TestTokenValidIfDateEqualsExpiry(t *testing.T) {
	expiry := time.Date(2017, 10, 25, 12, 25, 14, 9, time.UTC)
	token := Token{
		AccessToken:  "ACCESS",
		TokenType:    "TYPE",
		RefreshToken: "REFRESH",
		Expiry:       expiry,
	}
	assert.Equal(t, true, token.Valid(expiry))
}

func TestTokenInvalidIfAccesTokenIsEmpty(t *testing.T) {
	expiry := time.Date(2017, 10, 25, 12, 25, 14, 9, time.UTC)
	assert.Equal(t, false, Token{
		TokenType:    "TYPE",
		RefreshToken: "REFRESH",
		Expiry:       expiry,
	}.Valid(expiry))
	assert.Equal(t, false, Token{
		AccessToken:  "",
		TokenType:    "TYPE",
		RefreshToken: "REFRESH",
		Expiry:       expiry,
	}.Valid(expiry))
}

func TestWhenEnvHasAuth0InformationsThenReturnAuth0Authentication(t *testing.T) {
	env := MockingEnv{}
	env.On("String", "AUTH0_DOMAIN").Return("AUTH0_DOMAIN")
	env.On("String", "AUTH0_CLIENT_ID").Return("AUTH0_CLIENT_ID")
	env.On("String", "AUTH0_CLIENT_SECRET").Return("AUTH0_CLIENT_SECRET")
	env.On("String", "AUTH0_CALLBACK_URL").Return("https://my.url.com/callback")
	env.On("String", "AUTH0_AUDIENCE").Return("https://AUTH0_DOMAIN/api/v2/")
	stateGen := MockingStateGenerator{}
	logger := logrus.New()
	cookie := MockingCookies{}

	auth := Configure(cookie, env, logger, stateGen)
	assert.Equal(t, Auth0Authentication{cookie, stateGen, env, logger}, auth)
}

func TestWhenEnvIsEmptyThenReturnNoAuth(t *testing.T) {
	env := MockingEnv{}
	env.On("String", mock.Anything).Return("")
	stateGen := MockingStateGenerator{}
	logger := logrus.New()
	cookie := MockingCookies{}

	auth := Configure(cookie, env, logger, stateGen)
	assert.Equal(t, noAuth{cookie}, auth)
}
