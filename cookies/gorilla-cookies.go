package cookies

import (
	"net/http"
	"reflect"

	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
)

// GorillaSecureCookie implements Cookies wrapping Gorilla secure cookies
type GorillaSecureCookie struct {
	cookies *securecookie.SecureCookie
	logger  logrus.FieldLogger
}

func CreateCookie(conf Configuration, logger logrus.FieldLogger) *GorillaSecureCookie {
	result := new(GorillaSecureCookie)
	result.cookies = securecookie.New(conf.hashKey, conf.blockKey)
	result.logger = logger
	return result
}

func (c *GorillaSecureCookie) Set(res http.ResponseWriter, key string, value interface{}) string {
	encoded, err := c.cookies.Encode(key, value)
	if err != nil {
		c.logger.Warn("Impossible Cookie Encoding")
		return ""
	}
	http.SetCookie(res, &http.Cookie{
		Name:     key,
		Value:    encoded,
		HttpOnly: true,
	})
	return encoded
}

func (c *GorillaSecureCookie) Get(req *http.Request, key string, result interface{}) bool {
	encoded, err := req.Cookie(key)
	if err != nil {
		c.logger.WithField("cookie_key", key).Warn("Cookie not in Request")
		return false
	}
	err = c.cookies.Decode(key, encoded.Value, result)
	if err != nil {
		c.logger.WithFields(logrus.Fields{
			"cookie_key":           key,
			"cookie_expected_type": reflect.TypeOf(result),
		}).Warn("Cookie Bad Type")
		return false
	}
	return true
}
