package cookies

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

const hashKey = "hashkey"
const blockKey = "blockkey90123456"

var buffer *bytes.Buffer

func logger() *logrus.Entry {
	buffer = new(bytes.Buffer)
	result := logrus.New()
	result.SetLevel(logrus.DebugLevel)
	result.Out = buffer
	return result.WithField("application_version", "test")
}

func TestGorillaSecureCookieCanSetHeaderInResponse(t *testing.T) {
	res := httptest.NewRecorder()

	cookie := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger())
	encodedValue := cookie.Set(res, "key", "value")

	assert.Equal(t, "key="+encodedValue+"; HttpOnly", res.Header().Get("Set-Cookie"))
}

func TestCookieCanRetrieveValueInRequest(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	encodedValue := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Set(res, "key", "value")

	req.AddCookie(&http.Cookie{
		Name:  "key",
		Value: encodedValue,
	})

	value := ""
	exists := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Get(req, "key", &value)
	assert.Equal(t, "value", value)
	assert.True(t, exists)
}

type ComplexType struct {
	A string
	B int
}

func TestCookieCanStoreComplexType(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	expected := ComplexType{"hello", 1234}
	encodedValue := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Set(res, "key", expected)
	req.AddCookie(&http.Cookie{
		Name:  "key",
		Value: encodedValue,
	})

	actual := ComplexType{}
	exists := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Get(req, "key", &actual)
	assert.Equal(t, expected, actual)
	assert.True(t, exists)
}

func TestCookieCannotRetrieveValueIfNotPresentInRequest(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	value := ""
	exists := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Get(req, "key", &value)
	assert.Equal(t, "", value)
	assert.False(t, exists)
	assert.Contains(t, buffer.String(), "level=warning")
	assert.Contains(t, buffer.String(), "Cookie not in Request")
	assert.Contains(t, buffer.String(), "cookie_key=key")
	assert.Contains(t, buffer.String(), "application_version=test")
}

func TestCookieCannotRetrieveValueIfTypeNotCorresponding(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	expected := ComplexType{"hello", 1234}
	encodedValue := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Set(res, "key", expected)
	req.AddCookie(&http.Cookie{
		Name:  "key",
		Value: encodedValue,
	})

	value := ""
	exists := CreateCookie(Configuration{[]byte(hashKey), []byte(blockKey)}, logger()).Get(req, "key", &value)
	assert.Equal(t, "", value)
	assert.False(t, exists)
	assert.Contains(t, buffer.String(), "level=warning")
	assert.Contains(t, buffer.String(), "Cookie Bad Type")
	assert.Contains(t, buffer.String(), "cookie_key=key")
	assert.Contains(t, buffer.String(), "cookie_expected_type=\"*string\"")
	assert.Contains(t, buffer.String(), "application_version=test")
}

func TestLogIfEncodingIsImpossible(t *testing.T) {
	res := httptest.NewRecorder()
	// req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	cookie := CreateCookie(Configuration{[]byte("impossiblekey"), []byte("anotherimpossiblekey")}, logger())

	assert.Equal(t, "", cookie.Set(res, "key", "value"))

	assert.Contains(t, buffer.String(), "Impossible Cookie Encoding")
	assert.Contains(t, buffer.String(), "level=warning")
	assert.Contains(t, buffer.String(), "application_version=test")
}
