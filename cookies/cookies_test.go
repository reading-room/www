package cookies

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type mockingEnv struct {
	mock.Mock
}

func (m mockingEnv) String(str string) string {
	args := m.Called(str)
	return args.String(0)
}

func TestWhenConfigureWithNoHmacHashkeyThenReturnsDefaultHmacHashkey(t *testing.T) {
	e := mockingEnv{}
	e.On("String", "HMAC_HASHKEY").Return("")
	e.On("String", "AES_BLOCKKEY").Return("abcdefghijklmnopqrstuvwxyzabcdef")
	conf := Configure(e)
	assert.Equal(t, Configuration{[]byte("12345678901234567890123456789012"), []byte("abcdefghijklmnopqrstuvwxyzabcdef")}, conf)
}

func TestWhenConfigureWithNoBlockKeyThenReturnsDefaultBlockHashkey(t *testing.T) {
	e := mockingEnv{}
	e.On("String", "AES_BLOCKKEY").Return("")
	e.On("String", "HMAC_HASHKEY").Return("abcdefghijklmnopqrstuvwxyzabcdef")
	conf := Configure(e)
	assert.Equal(t, Configuration{[]byte("abcdefghijklmnopqrstuvwxyzabcdef"), []byte("12345678901234567890123456789012")}, conf)
}

func TestWhenConfigureWithBlockKeyAndHashkeyThenDoNotUseDefault(t *testing.T) {
	e := mockingEnv{}
	e.On("String", "AES_BLOCKKEY").Return("fedcbazyxwvutsrqponmlkjihgfedcba")
	e.On("String", "HMAC_HASHKEY").Return("abcdefghijklmnopqrstuvwxyzabcdef")
	conf := Configure(e)
	assert.Equal(t, Configuration{[]byte("abcdefghijklmnopqrstuvwxyzabcdef"), []byte("fedcbazyxwvutsrqponmlkjihgfedcba")}, conf)
}
