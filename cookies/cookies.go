package cookies

import (
	"net/http"

	"gitlab.com/reading-room/www/env"
)

type Cookies interface {
	Get(*http.Request, string, interface{}) bool
	Set(res http.ResponseWriter, key string, value interface{}) string
}

// CookiesConfiguration contains key for secure cookies
type Configuration struct {
	hashKey  []byte
	blockKey []byte
}

const defaultKey = "12345678901234567890123456789012"

// Configure Cookies according to environment variables
func Configure(e env.Env) Configuration {
	return Configuration{getEnvWithDefault(e, "HMAC_HASHKEY"), getEnvWithDefault(e, "AES_BLOCKKEY")}
}

func getEnvWithDefault(e env.Env, key string) []byte {
	value := e.String(key)
	if value == "" {
		return []byte(defaultKey)
	}
	return []byte(value)
}
