package main

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/reading-room/www/cookies"
	"gitlab.com/reading-room/www/epub"
	"gitlab.com/reading-room/www/oauth"
)

const EpubId = "epub-id-123"
const ResourceId = "resource-id-456"
const ContentRefId = "ref-id-789"

type fakeNavPoint struct {
	id       string
	ref      string
	text     string
	children []epub.NavPoint
}

func (f fakeNavPoint) Id() string {
	return f.id
}
func (f fakeNavPoint) NavPoints() []epub.NavPoint {
	return f.children
}
func (f fakeNavPoint) ContentRef() string {
	return f.ref
}
func (f fakeNavPoint) Text() string {
	return f.text
}

type fakeNcx struct {
	navpoints []epub.NavPoint
}

func (n fakeNcx) NavPoints() []epub.NavPoint {
	return n.navpoints
}

type fakeAuthor struct {
	name string
}

func (a fakeAuthor) Name() string {
	return a.name
}
func (a fakeAuthor) Role() string {
	return ""
}

type FakeEPub struct {
	id      string
	titles  []string
	authors []string
	ncx     epub.Ncx
}

func (f FakeEPub) Ncx() epub.Ncx {
	if f.ncx != nil {
		return f.ncx
	}
	return fakeNcx{navpoints: []epub.NavPoint{fakeNavPoint{id: ResourceId, ref: ContentRefId}}}
}
func (f FakeEPub) Title() []string {
	if f.titles == nil {
		return []string{}
	}
	return f.titles
}
func (f FakeEPub) Id() string {
	return f.id
}
func (f FakeEPub) Author() []epub.Author {
	result := []epub.Author{}
	for _, a := range f.authors {
		result = append(result, fakeAuthor{a})
	}
	return result
}

func (f FakeEPub) Get(ref epub.ResourceReference) (io.ReadCloser, error) {
	if ref.Get() == ContentRefId {
		return ioutil.NopCloser(strings.NewReader("RESOURCE")), nil
	}
	return nil, errors.New("Bad resource access")
}

type MockingAuthentication struct {
	mock.Mock
}

func (m *MockingAuthentication) IsAuthenticated(r *http.Request, time time.Time) bool {
	args := m.Called()
	return args.Bool(0)
}
func (m *MockingAuthentication) LoginHandler(w http.ResponseWriter, r *http.Request) {
	m.Called(w, r)
}
func (m *MockingAuthentication) CallbackHandler(w http.ResponseWriter, r *http.Request) {
	m.Called(w, r)
}

func newMockingAuthentication(b bool) *MockingAuthentication {
	notAuthenticated := new(MockingAuthentication)
	notAuthenticated.On("IsAuthenticated").Return(b)
	return notAuthenticated
}
func notAuthenticated() *MockingAuthentication {
	return newMockingAuthentication(false)
}
func authenticated() *MockingAuthentication {
	return newMockingAuthentication(true)
}

func now() time.Time {
	return time.Date(2017, time.August, 1, 16, 37, 25, 13, time.UTC)
}

type MockingCookies struct {
	mock.Mock
}

func (c *MockingCookies) Get(r *http.Request, k string, v interface{}) bool {
	args := c.Called(r, k, v)
	return args.Bool(0)
}
func (c *MockingCookies) Set(res http.ResponseWriter, key string, value interface{}) string {
	args := c.Called(res, key, value)
	return args.String(0)
}
func emptyCookies() cookies.Cookies {
	return &MockingCookies{}
}
func cookiesWithUsername(username string) *MockingCookies {
	cookies := MockingCookies{}
	cookies.On("Get", mock.AnythingOfType("*http.Request"), "user-info", &oauth.UserInfo{}).Run(func(args mock.Arguments) {
		userInfo := args.Get(2).(*oauth.UserInfo)
		userInfo.Nickname = username
	}).Return(true)
	return &cookies
}

type MockingEPubList struct {
	mock.Mock
}

func (m *MockingEPubList) List() []epub.Thumbnail {
	args := m.Called()
	return args.Get(0).([]epub.Thumbnail)
}
func (m *MockingEPubList) All() []epub.EPub {
	args := m.Called()
	return args.Get(0).([]epub.EPub)
}
func (m *MockingEPubList) Load(id string) epub.EPub {
	args := m.Called(id)
	return args.Get(0).(epub.EPub)
}

type NoopEPubList struct {
}

func (l NoopEPubList) List() []epub.Thumbnail {
	return []epub.Thumbnail{}
}
func (l NoopEPubList) All() []epub.EPub {
	return []epub.EPub{}
}
func (l NoopEPubList) Load(id string) epub.EPub {
	return nil
}

var logfile *bytes.Buffer

func logger() *logrus.Entry {
	logfile = new(bytes.Buffer)
	result := logrus.New()
	result.SetLevel(logrus.DebugLevel)
	result.Out = logfile
	return result.WithField("test", "ok")
}

func TestFavicon(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/favicon.ico", nil)
	Serve(notAuthenticated(), now(), emptyCookies(), NoopEPubList{}, logger()).ServeHTTP(res, req)
	if res.Code != 200 {
		t.Fatalf("Expected 200 but was %d", res.Code)
	}
	expected, _ := ioutil.ReadFile("assets/favicon.ico")
	assert.Equal(t, expected, res.Body.Bytes())
	assert.Contains(t, logfile.String(), "AccessLog path=/favicon.ico")
}

func TestHealthcheck(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/v1/healthcheck", nil)
	Serve(notAuthenticated(), now(), emptyCookies(), NoopEPubList{}, logger()).ServeHTTP(res, req)
	assert.Equal(t, 200, res.Code, "Expected OK (200)")
	body, _ := ioutil.ReadAll(res.Body)
	assert.Equal(t, "hello internal voice", string(body))
	assert.Contains(t, logfile.String(), "AccessLog path=/v1/healthcheck")
}

func TestGoToLoginIfNotAuthenticated(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	Serve(notAuthenticated(), now(), emptyCookies(), NoopEPubList{}, logger()).ServeHTTP(res, req)
	assert.Equal(t, 302, res.Code, "Expected redirection (302)")
	assert.Equal(t, "/login", res.HeaderMap["Location"][0])
	assert.Contains(t, logfile.String(), "AccessLog path=/")
}

func TestGoToIndexIfAuthenticated(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)

	cookies := MockingCookies{}
	cookies.On("Get", mock.AnythingOfType("*http.Request"), "user-info", &oauth.UserInfo{}).Run(func(args mock.Arguments) {
		userInfo := args.Get(2).(*oauth.UserInfo)
		userInfo.Nickname = "UserNickName"
	}).Return(true)

	Serve(authenticated(), now(), &cookies, NoopEPubList{}, logger()).ServeHTTP(res, req)
	assert.Equal(t, 200, res.Code)

	assert.Contains(t, res.Body.String(), "<title>ReadingRoom</title>")
	assert.Contains(t, res.Body.String(), "Welcome UserNickName!")
	assert.Contains(t, logfile.String(), "AccessLog path=/")
}

func TestLoginCallsLoginHandler(t *testing.T) {
	authentication := new(MockingAuthentication)
	authentication.On("LoginHandler", mock.AnythingOfType("*httptest.ResponseRecorder"), mock.AnythingOfType("*http.Request")).Once()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/login", nil)
	Serve(authentication, now(), emptyCookies(), NoopEPubList{}, logger()).ServeHTTP(res, req)

	authentication.AssertExpectations(t)
	assert.Contains(t, logfile.String(), "AccessLog path=/login")
}

func TestCallbackCallsCallbackHandler(t *testing.T) {
	authentication := new(MockingAuthentication)
	authentication.On("CallbackHandler", mock.AnythingOfType("*httptest.ResponseRecorder"), mock.AnythingOfType("*http.Request")).Once()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/callback", nil)

	Serve(authentication, now(), emptyCookies(), NoopEPubList{}, logger()).ServeHTTP(res, req)

	authentication.AssertExpectations(t)
	assert.Contains(t, logfile.String(), "AccessLog path=/callback")
}

func TestAccessUnknownUrlLogsInError(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/unknown/url", nil)

	cookies := MockingCookies{}
	cookies.On("Get", mock.AnythingOfType("*http.Request"), "user-info", &oauth.UserInfo{}).Run(func(args mock.Arguments) {
		userInfo := args.Get(2).(*oauth.UserInfo)
		userInfo.Nickname = "UserNickName"
	}).Return(true)

	Serve(authenticated(), now(), &cookies, &NoopEPubList{}, logger()).ServeHTTP(res, req)
	assert.Equal(t, 404, res.Code)
	assert.Contains(t, logfile.String(), "AccessLog path=/unknown/url")
	assert.Contains(t, logfile.String(), "level=error")
}

func TestHomeTemplaterDisplayListOfBooks(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	epubList := MockingEPubList{}
	epubList.On("All").Return([]epub.EPub{
		FakeEPub{id: "123456", titles: []string{"Title 1"}, authors: []string{"Author 1"}},
		FakeEPub{id: "789012", titles: []string{"Title 2"}, authors: []string{"Author 2"}},
	})

	Serve(authenticated(), now(), cookiesWithUsername("User1"), &epubList, logger()).ServeHTTP(res, req)

	epubList.AssertExpectations(t)
	assert.Contains(t, res.Body.String(), "Welcome User1!")

	assert.Regexp(t, regexp.MustCompile(`(?ms:<li><a href="v1/read/123456">Title 1, by Author 1</a></li>\s*<li><a href="v1/read/789012">Title 2, by Author 2</a></li>)`), res.Body.String())
}
func TestDisplayTableOfContentOnEpubHome(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8080/v1/read/123456", nil)
	epubList := MockingEPubList{}
	epubList.On("List").Return([]epub.Thumbnail{
		epub.Thumbnail{Id: "123456", Title: "Title 1", Author: "Author 1"},
	})
	testingEpub := FakeEPub{
		id:     "123456",
		titles: []string{"Title 1"},
		ncx: fakeNcx{
			navpoints: []epub.NavPoint{
				fakeNavPoint{id: "np-1", ref: "chap-1", text: "Chapter 1",
					children: []epub.NavPoint{
						fakeNavPoint{id: "np-11", ref: "chap-11", text: "Chapter 1.1"},
					},
				},
				fakeNavPoint{id: "np-2", ref: "chap-2", text: "Chapter 2"},
			},
		},
	}
	epubList.On("Load", "123456").Return(testingEpub)

	Serve(authenticated(), now(), cookiesWithUsername("User1"), &epubList, logger()).ServeHTTP(res, req)

	assert.Equal(t, 200, res.Code)
	assert.Regexp(t, regexp.MustCompile(`(?ms:<ul>\s*<li>Chapter 1</li>\s*<li>Chapter 1.1</li>\s*<li>Chapter 2</li>\s*</ul>)`), res.Body.String())
}
