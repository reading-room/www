# WWW

Web listener service in the `reading-room` project.

Wil create docker image `registry.gitlab.com/reading-room/www:1-show-epub-resource-on-url-full-web-with-tests`

# Run

## Build and Run

```
docker build -t readingroom .
docker run --env-file setup_env --rm -ti -p 8080:8080 readingroom
```

## Development time

```
docker run --name minio1 -d minio/minio server /data

docker run --rm -ti -u 1000 -v $(pwd):/go golang:1.9.1 go test
docker run --rm -ti -u 1000  --env-file setup_env -v $(pwd):/go/src/gitlab.com/reading-room/www -w /go/src/gitlab.com/reading-room/www --link minio1:minio -p 8080:8080 golang:1.9.1 go run main.go
```

## Environment

Injected variables:
* AUTH0_DOMAIN
* AUTH0_CLIENT_ID
* AUTH0_CLIENT_SECRET
* AUTH0_CALLBACK_URL
* COOKIE_STORE_KEY
* HMAC_HASHKEY  
* AES_BLOCKKEY
* DISCORD_WEBHOOK_URL : in format LEVEL,URL|LEVEL,URL...
* APPLICATION_VERSION
* MINIO_ACCESS_KEY
* MINIO_SECRET_ACCESS_KEY
