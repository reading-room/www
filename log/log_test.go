package log

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockingEnv struct {
	mock.Mock
}

func (m MockingEnv) String(k string) string {
	return m.Called(k).String(0)
}

func TestCreateDiscordrusWebHook(t *testing.T) {
	webhook := NewDiscordrusHook(logrus.FatalLevel, "https://url/")
	assert.Equal(t, "https://url/", webhook.WebhookURL)
	assert.Equal(t, logrus.FatalLevel, webhook.MinLevel)
}

func setupEnv(m map[string]string) MockingEnv {
	env := MockingEnv{}
	for k := range m {
		env.On("String", k).Return(m[k])
	}
	return env
}

func TestConfigLoggerFullTimestamp(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "",
		"APPLICATION_VERSION": "",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.True(t,
		logger.Logger.Formatter.(*logrus.TextFormatter).FullTimestamp)
}

func TestConfigWebHookIfUrl(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "INFO,https://discord/webook",
		"APPLICATION_VERSION": "",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.Equal(t, 1, len(logger.Logger.Hooks[logrus.InfoLevel]))
	assert.Equal(t,
		NewDiscordrusHook(logrus.InfoLevel, "https://discord/webook"),
		logger.Logger.Hooks[logrus.InfoLevel][0])
}

func TestConfigManyWebHooksIfManyUrl(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "INFO,https://discord/webook|WARN,https://discord/webook-warn",
		"APPLICATION_VERSION": "",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.Equal(t, 1, len(logger.Logger.Hooks[logrus.InfoLevel]))
	assert.Equal(t, 2, len(logger.Logger.Hooks[logrus.WarnLevel]))
	assert.Equal(t,
		NewDiscordrusHook(logrus.InfoLevel, "https://discord/webook"),
		logger.Logger.Hooks[logrus.InfoLevel][0])
	assert.Equal(t,
		NewDiscordrusHook(logrus.InfoLevel, "https://discord/webook"),
		logger.Logger.Hooks[logrus.WarnLevel][0])
	assert.Equal(t,
		NewDiscordrusHook(logrus.WarnLevel, "https://discord/webook-warn"),
		logger.Logger.Hooks[logrus.WarnLevel][1])
}

func TestNoWebHookIfNoUrl(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "",
		"APPLICATION_VERSION": "",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.Equal(t,
		0,
		len(logger.Logger.Hooks[logrus.InfoLevel]))
}

func TestUnknownLevelDefaultToDebug(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "SKRIA,https://discord/webook",
		"APPLICATION_VERSION": "",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.Equal(t, 1, len(logger.Logger.Hooks[logrus.InfoLevel]))
	assert.Equal(t,
		NewDiscordrusHook(logrus.InfoLevel, "https://discord/webook"),
		logger.Logger.Hooks[logrus.InfoLevel][0])
}

func TestConfigWithFieldVersionIfVersionSetInEnv(t *testing.T) {
	env := setupEnv(map[string]string{
		"DISCORD_WEBHOOK_URL": "",
		"APPLICATION_VERSION": "testing",
	})
	logger := InitLogger(env).(*logrus.Entry)
	assert.Equal(t,
		"testing",
		logger.Data["version"])
}
