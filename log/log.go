package log

import (
	"strings"

	"github.com/kz/discordrus"
	"github.com/sirupsen/logrus"
	"gitlab.com/reading-room/www/env"
)

func NewDiscordrusHook(level logrus.Level, url string) *discordrus.Hook {
	return discordrus.NewHook(
		// Use environment variable for security reasons
		url,
		// Set minimum level to DebugLevel to receive all log entries
		level,
		&discordrus.Opts{
			Username:           "ReadingRoomLogs",
			Author:             "",                                 // Setting this to a non-empty string adds the author text to the message header
			DisableTimestamp:   false,                              // Setting this to true will disable timestamps from appearing in the footer
			TimestampFormat:    "2006 01 02 15:04:05.00000 -07:00", // The timestamp takes this format; if it is unset, it will take logrus' default format
			EnableCustomColors: true,                               // If set to true, the below CustomLevelColors will apply
			CustomLevelColors: &discordrus.LevelColors{
				Debug: 10170623,
				Info:  3581519,
				Warn:  14327864,
				Error: 13631488,
				Panic: 13631488,
				Fatal: 13631488,
			},
			DisableInlineFields: false, // If set to true, fields will not appear in columns ("inline")
		},
	)
}

func InitLogger(env env.Env) logrus.FieldLogger {
	logger := logrus.New()
	logger.Formatter = &logrus.TextFormatter{FullTimestamp: true, TimestampFormat: "2006 01 02 15:04:05.000 -07:00"}
	if config := env.String("DISCORD_WEBHOOK_URL"); config != "" {
		for _, conf := range strings.Split(config, "|") {
			configArray := strings.Split(conf, ",")
			level, errlevel := logrus.ParseLevel(configArray[0])
			if errlevel != nil {
				level = logrus.InfoLevel
			}
			logger.AddHook(NewDiscordrusHook(level, configArray[1]))
		}
	}
	if version := env.String("APPLICATION_VERSION"); version != "" {
		return logger.WithField("version", env.String("APPLICATION_VERSION"))
	}
	return &logrus.Entry{Logger: logger}
}
